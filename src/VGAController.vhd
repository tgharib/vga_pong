library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity VGAController is
	port (
		clk : in std_logic;
		SW0, SW1, SW2, SW3 : in std_logic;
		DAC_CLK : out std_logic;
		H : out std_logic;
		V : out std_logic;
		Bout : out std_logic_vector (7 downto 0);
		Gout : out std_logic_vector (7 downto 0);
		Rout : out std_logic_vector (7 downto 0)
	);
end VGAController;

architecture Behavioral of VGAController is
	-- duplicate of port signals as internal signals
	signal signal_Rout, signal_Bout, signal_Gout : std_logic_vector(7 downto 0) := "00000000";
	signal signal_HSync, signal_VSync, signal_DAC_CLK : std_logic := '0';

	-- x and y coordinates point to the current pixel being drawn
	-- HSyncCycleCounter counts cycles and VSyncLineCounter counts lines
	signal x, y, VSyncLineCounter, HSyncCycleCounter : integer := 0;

	-- counters slow down the paddles and the ball
	signal pad1Counter, pad2Counter, ballCounter : integer;

	-- y position for paddles
	signal pad1Y : integer := 240; -- paddle one is left paddle
	signal pad2Y : integer := 240; -- paddle two is right paddle

	-- constants controlling paddle
	constant paddle_velocity : integer := 2;
	constant paddle_width : integer := 30;
	constant paddle_thickness : integer := 10;
	constant pad1X : integer := 60;
	constant pad2X : integer := 570;

	-- ball x and y coordinate
	signal ballX : integer := 320;
	signal ballY : integer := 240;

	-- ball motion vector
	signal ballMotionX : integer := 1;
	signal ballMotionY : integer := 1;

	-- constant controlling ball_radius
	constant ball_radius : integer := 5;

	-- pause to freeze the ball after a goal is made and to change the color of ball
	signal pause : integer := 0;
	signal collision_flag : integer := 1;
	signal scored_flag : integer := 0;

	-- edges of active area
	constant bottom_edge : integer := 450;
	constant top_edge : integer := 30;
	constant left_edge : integer := 40;
	constant right_edge : integer := 600;
	constant goaltop : integer := 160;
	constant goalbottom : integer := 320;

	constant border_thickness : integer := 15;

	-- chipscope
	component icon
		port (
			CONTROL0 : inout std_logic_vector(35 downto 0));
	end component;

	component ila
		port (
			CONTROL : inout std_logic_vector(35 downto 0);
			CLK : in std_logic;
			DATA : in std_logic_vector(87 downto 0);
			TRIG0 : in std_logic_vector(7 downto 0));
	end component;

	signal control0 : std_logic_vector(35 downto 0);
	signal ila_data : std_logic_vector(87 downto 0);
	signal trig0 : std_logic_vector(7 downto 0);

begin
	-- connect internal signals to port signals
	DAC_CLK <= signal_DAC_CLK;
	Rout <= signal_Rout;
	Gout <= signal_Gout;
	Bout <= signal_Bout;
	H <= signal_HSync;
	V <= signal_VSync;

	-- 1 for horizontal pixels (640 cycles) then 1 for front porch (16 cycles) then 0 for sync pulse (96 cycles) then 1 for back porch (48 cycles)
	signal_HSync <= '1' when (HSyncCycleCounter < 640 + 16) or (HSyncCycleCounter > 640 + 16 + 96)
		else '0';

	-- 1 for 480 lines then 1 for front porch (10 lines) then 0 for sync pulse (2 lines) then 1 for back porch (33 lines)
	signal_VSync <= '1' when (VSyncLineCounter < 480 + 10) or (VSyncLineCounter > 480 + 10 + 2)
		else '0';

	x <= HSyncCycleCounter when (HSyncCycleCounter < 640)
		else x;

	y <= VSyncLineCounter when (VSyncLineCounter < 480)
		else x;

	-- PixClock is twice as slow as clk
	process (clk)
	begin
		if (rising_edge(clk)) then
			signal_DAC_CLK <= not(signal_DAC_CLK);
		end if;
	end process;

	process (signal_DAC_CLK)
	begin
		if (rising_edge(signal_DAC_CLK)) then
			-- count cycles and lines
			if (HSyncCycleCounter < 800 - 1) then
				HSyncCycleCounter <= HSyncCycleCounter + 1;
			else
				HSyncCycleCounter <= 0;
				if (VSyncLineCounter < 525 - 1) then
					VSyncLineCounter <= VSyncLineCounter + 1;
				else
					VSyncLineCounter <= 0;
				end if;
			end if;

			-- paddle movement
			-- SW0: Player 1 UP, SW1: Player 1 DOWN
			pad1Counter <= pad1Counter + 1;
			if (pad1Counter > 500000) then -- slow down the paddle movement
				pad1Counter <= 0;
				if ((SW0 xor SW1) = '1') then
					if (SW0 = '1' and pad1Y - paddle_width > top_edge) then -- prevent the paddle from crossing the borders
						pad1Y <= pad1Y - paddle_velocity;
					elsif (SW1 = '1' and pad1Y + paddle_width < bottom_edge) then -- prevent the paddle from crossing the borders
						pad1Y <= pad1Y + paddle_velocity;
					end if;
				end if;
			end if;

			-- SW2: Player 2 UP, SW3: Player 2 DOWN
			pad2Counter <= pad2Counter + 1;
			if (pad2Counter > 500000) then -- slow down the paddle movement
				pad2Counter <= 0;
				if ((SW2 xor SW3) = '1') then
					if (SW2 = '1' and pad2Y - paddle_width > top_edge) then -- prevent the paddle from crossing the borders
						pad2Y <= pad2Y - paddle_velocity;
					elsif (SW3 = '1' and pad2Y + paddle_width < bottom_edge) then -- prevent the paddle from crossing the borders
						pad2Y <= pad2Y + paddle_velocity;
					end if;
				end if;
			end if;

			-- ball movement
			ballCounter <= ballCounter + 1;
			if (ballCounter > 200000) then
				ballCounter <= 0;

				if (scored_flag = 0 and ((((ballX + ballMotionX - ball_radius) < left_edge) and (((ballY + ballMotionY - ball_radius) > goaltop) and ((ballY + ballMotionY + ball_radius) < goalbottom))) or (((ballX + ballMotionX + ball_radius) > right_edge) and (((ballY + ballMotionY - ball_radius) > goaltop) and ((ballY + ballMotionY + ball_radius) < goalbottom))))) then
					-- check if someone has scored
					scored_flag <= 1;
				elsif (scored_flag = 1) then
					pause <= pause + 1;
					collision_flag <= 0;
					ballX <= ballX + ballMotionX;
					ballY <= ballY + ballMotionY;
					if (pause > 30) then
						ballX <= 320;
						ballY <= 240;
						pause <= 0;
						collision_flag <= 1;
						scored_flag <= 0;
					end if;
				elsif (scored_flag = 0) then
					if ((((ballX + ballMotionX - ball_radius < pad1X + paddle_thickness) and (ballX + ballMotionX + ball_radius > pad1X)) and ((pad1Y + paddle_width > ballY + ballMotionY + ball_radius) and (pad1Y - paddle_width < ballY + ballMotionY - ball_radius))) or (((ballX + ballMotionX + ball_radius > pad2X) and (ballX + ballMotionX - ball_radius < pad2X + paddle_thickness)) and ((pad2Y + paddle_width > ballY + ballMotionY + ball_radius) and (pad2Y - paddle_width < ballY + ballMotionY - ball_radius)))) then
						-- check for collision between ball and paddles (back and front)
						ballMotionX <= - ballMotionX;
					else
						-- check for collision with borders
						if (collision_flag = 1 and ((ballX + ballMotionX + ball_radius > right_edge) or (ballX + ballMotionX - ball_radius < left_edge))) then
							ballMotionX <= - ballMotionX;
						elsif (collision_flag = 1 and (ballY + ballMotionY + ball_radius > bottom_edge or ballY + ballMotionY - ball_radius < top_edge)) then
							ballMotionY <= - ballMotionY;
						end if;
					end if;
					-- update the ball condition with the ball motion vector
					ballX <= ballX + ballMotionX;
					ballY <= ballY + ballMotionY;
				end if;
			end if;
		end if; -- end if (rising_edge(signal_DAC_CLK))

		-- background drawing
		if (VSyncLineCounter < 480 and HSyncCycleCounter < 640) then -- active region
			if ((y < 5) or (y > 475)) then
				-- create blue border
				signal_Rout <= "00000000";
				signal_Gout <= "00000000";
				signal_Bout <= "11111111";
			elsif (((y > top_edge - border_thickness and y < top_edge) and (x > left_edge - border_thickness and x < right_edge + border_thickness)) or ((y < bottom_edge + border_thickness and y > bottom_edge) and (x > left_edge - border_thickness and x < right_edge + border_thickness)) or (((y > top_edge - border_thickness and y < goaltop) or (y > goalbottom and y < bottom_edge + border_thickness)) and (x > top_edge and x < top_edge + border_thickness)) or (((y > top_edge - border_thickness and y < goaltop) or (y > goalbottom and y < bottom_edge + border_thickness)) and (x > right_edge and x < right_edge + border_thickness))) then
				-- create white barriers
				signal_Rout <= "11111111";
				signal_Gout <= "11111111";
				signal_Bout <= "11111111";
			elsif ((x > 319 and x < 321) and ((y > 60 and y < 100) or (y > 140 and y < 180) or (y > 220 and y < 260) or (y > 300 and y < 340) or (y > 380 and y < 420))) then
				-- create black dashed line in centre
				signal_Rout <= "00000000";
				signal_Gout <= "00000000";
				signal_Bout <= "00000000";
			elsif ((y > (pad1Y - paddle_width) and y < (pad1Y + paddle_width)) and (x > pad1X and x < pad1X + paddle_thickness)) then
				-- draw paddle 1 - blue
				signal_Bout <= "11111111";
				signal_Rout <= "00000000";
				signal_Gout <= "00000000";
			elsif ((y > (pad2Y - paddle_width) and y < (pad2Y + paddle_width)) and (x > pad2X and x < pax2X + paddle_thickness)) then
				-- draw paddle 2 - pink
				signal_Bout <= "00001111";
				signal_Rout <= "11111001";
				signal_Gout <= "00001000";
			elsif (((y > (ballY - ball_radius)) and (y < (ballY + ball_radius))) and ((x > (ballX - ball_radius)) and (x < (ballX + ball_radius)))) then
				-- draw the ball
				if (pause > 0) then
					-- change ball colour to red after scoring
					signal_Rout <= "11111111";
					signal_Gout <= "00000000";
					signal_Bout <= "00000000";
				else
					-- ball is active - yellow
					signal_Rout <= "11111111";
					signal_Gout <= "11111111";
					signal_Bout <= "00000000";
				end if;
			else
				-- make all other pixels green
				signal_Rout <= "00000000";
				signal_Gout <= "11111111";
				signal_Bout <= "00000000";
			end if;
		else -- region outside of display is black
			signal_Rout <= "00000000";
			signal_Gout <= "00000000";
			signal_Bout <= "00000000";
		end if;
	end process;

	-- chipscope
	projectIcon : icon port map(control0);
	projectIla : ila port map(control0, clk, ila_data, trig0);

	ila_data(9 downto 0) <= std_logic_vector(to_unsigned(HSyncCycleCounter, 10));
	ila_data(19 downto 10) <= std_logic_vector(to_unsigned(VSyncLineCounter, 10));
	ila_data(20) <= signal_HSync;
	ila_data(21) <= signal_VSync;
	ila_data(22) <= signal_DAC_CLK;
	ila_data(30 downto 23) <= signal_Rout;
	ila_data(38 downto 31) <= signal_Gout;
	ila_data(46 downto 39) <= signal_Bout;
	ila_data(56 downto 47) <= std_logic_vector(to_unsigned(pad1Y, 10));
	ila_data(66 downto 57) <= std_logic_vector(to_unsigned(pad2Y, 10));
	ila_data(73 downto 67) <= std_logic_vector(to_unsigned(ballX, 7));
	ila_data(80 downto 74) <= std_logic_vector(to_unsigned(ballY, 7));
	ila_data(87 downto 81) <= (others => '0'); -- unused

	trig0(0) <= SW0;
	trig0(1) <= SW1;
	trig0(2) <= SW2;
	trig0(3) <= SW3;
	trig0(7 downto 4) <= (others => '0'); -- unused
end Behavioral;
